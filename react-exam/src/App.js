import React, { useState } from 'react';

function Hello(){
  const [text, setText] = useState(' ');
  const handleChange=(event) =>{
    setText(event.target.value);
  }
  return(
    <div className="car">
      <p>Enter you name:{text}</p>
      <input type="text" value={text} onChange={handleChange}></input>
      <button value="submit">Submit</button>
    </div>
  );

}

function Cars({value}){
  const list = value.map((value,index)=>{
    return <li key={index}>I am a {value}</li>
  });

  
  
  return(
    <div className="cars">
    <h1>Who lives in my garage?</h1>
    <ul>
      {list}
    </ul>
    </div>
  );

}



function App() {
  const cars=['Ford','BMW','Audi'];
  return (
    <div>
    <Hello />

    <Cars value={cars}/>
    </div>
  );
}

export default App;
